/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lib.namedBean;

import java.io.Serializable;
import java.util.List;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.http.HttpSession;
import com.lib.jpa.Customer;
import javax.inject.Inject;

/**
 *
 * @author Евгений
 */
@Named
@SessionScoped
public class LoginBean implements Serializable {

    private Customer currentUser;
    private String login;
    private String password;
    private boolean isLogin;
    @Inject
    private com.lib.jsf.CustomerController customerContoller;

    public String login() {
        boolean flag = false;
        List<Customer> list = customerContoller.getItems();

        for (com.lib.jpa.Customer item : list) {
            if (item.getPassword().equals(getPassword()) && item.getEmail().equals(getLogin())) {
                currentUser = item;
                isLogin = true;
                flag = true;
            }
        }

        if (flag) {
            HttpSession session = SessionUtils.getSession();
            session.setAttribute("username", currentUser.getRealname());
            session.setAttribute("email", currentUser.getEmail());
            session.setAttribute("userId", currentUser.getIdCustomer());
            return "welcomePrimefaces";
        } else {
            FacesContext.getCurrentInstance().addMessage("errorLoginMessage",
                    new FacesMessage("Неправильный логин или пароль"));
            return "login";
        }
    }

    public String logout() {
        HttpSession session = SessionUtils.getSession();
        session.invalidate();
        isLogin = false;
        return "index";
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getLogin() {
        return this.login;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the isLogin
     */
    public boolean isIsLogin() {
        return isLogin;
    }

    /**
     * @return the currentUser
     */
    public Customer getCurrentUser() {
        return currentUser;
    }

    /**
     * @param currentUser the currentUser to set
     */
    public void setCurrentUser(Customer currentUser) {
        this.currentUser = currentUser;
    }

}
