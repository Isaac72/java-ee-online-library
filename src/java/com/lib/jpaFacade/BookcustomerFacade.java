/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lib.jpaFacade;

import com.lib.jpa.Bookcustomer;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Евгений
 */
@Stateless
public class BookcustomerFacade extends AbstractFacade<Bookcustomer> {

    @PersistenceContext(unitName = "onlinelibraryPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public BookcustomerFacade() {
        super(Bookcustomer.class);
    }
    
    public List<Bookcustomer> findByPrimaryKey(int bookId, int customerId){
        Query query = getEntityManager().createNamedQuery("Bookcustomer.findByPrimaryKey");
        query.setParameter("customerId", customerId);
        query.setParameter("bookId", bookId);
        
        return query.getResultList();
    } 
    public void deleteByKeys(int bookId, int customerId){
        Query query = getEntityManager().createNamedQuery("Bookcustomer.deleteByKeys");
        query.setParameter("customerId", customerId);
        query.setParameter("bookId", bookId);
        query.executeUpdate();
    }
    
}
