/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lib.jpa;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Евгений
 */
@Entity
@Table(name = "bookcustomer")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Bookcustomer.findAll", query = "SELECT b FROM Bookcustomer b"),
    @NamedQuery(name = "Bookcustomer.findByCustomerId", query = "SELECT b FROM Bookcustomer b WHERE b.bookcustomerPK.customerId = :customerId"),
    @NamedQuery(name = "Bookcustomer.findByBookId", query = "SELECT b FROM Bookcustomer b WHERE b.bookcustomerPK.bookId = :bookId"),
    @NamedQuery(name = "Bookcustomer.findByPrimaryKey", query = "SELECT b FROM Bookcustomer b WHERE b.bookcustomerPK.bookId = :bookId AND b.bookcustomerPK.customerId = :customerId"),
    @NamedQuery(name = "Bookcustomer.findByAdddate", query = "SELECT b FROM Bookcustomer b WHERE b.adddate = :adddate"),
    @NamedQuery(name = "Bookcustomer.deleteByKeys", query = "DELETE FROM Bookcustomer b WHERE b.bookcustomerPK.bookId = :bookId AND b.bookcustomerPK.customerId = :customerId")})
public class Bookcustomer implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected BookcustomerPK bookcustomerPK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "adddate")
    @Temporal(TemporalType.DATE)
    private Date adddate;
    @JoinColumn(name = "book_id", referencedColumnName = "id_book", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Book book;
    @JoinColumn(name = "customer_id", referencedColumnName = "id_customer", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Customer customer;

    public Bookcustomer() {
    }

    public Bookcustomer(BookcustomerPK bookcustomerPK) {
        this.bookcustomerPK = bookcustomerPK;
    }

    public Bookcustomer(BookcustomerPK bookcustomerPK, Date adddate) {
        this.bookcustomerPK = bookcustomerPK;
        this.adddate = adddate;
    }

    public Bookcustomer(int customerId, int bookId) {
        this.bookcustomerPK = new BookcustomerPK(customerId, bookId);
    }

    public BookcustomerPK getBookcustomerPK() {
        return bookcustomerPK;
    }

    public void setBookcustomerPK(BookcustomerPK bookcustomerPK) {
        this.bookcustomerPK = bookcustomerPK;
    }

    public Date getAdddate() {
        return adddate;
    }

    public void setAdddate(Date adddate) {
        this.adddate = adddate;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (bookcustomerPK != null ? bookcustomerPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Bookcustomer)) {
            return false;
        }
        Bookcustomer other = (Bookcustomer) object;
        if ((this.bookcustomerPK == null && other.bookcustomerPK != null) || (this.bookcustomerPK != null && !this.bookcustomerPK.equals(other.bookcustomerPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lib.jpaFacade.Bookcustomer[ bookcustomerPK=" + bookcustomerPK + " ]";
    }
    
}
