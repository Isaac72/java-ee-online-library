/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lib.jsf;

import com.lib.jpa.Book;
import com.lib.jpa.Bookcustomer;
import com.lib.jpa.BookcustomerPK;
import com.lib.jpa.Customer;
import com.lib.jpaFacade.CustomerFacade;
import com.lib.namedBean.SessionUtils;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Евгений
 */
@Named
@SessionScoped
public class CustomerController implements Serializable {

    @EJB
    private com.lib.jpaFacade.CustomerFacade ejbFacade;
    @EJB
    private com.lib.jpaFacade.BookcustomerFacade bookCustomerFacade;
    @Inject
    private com.lib.namedBean.LoginBean loginBean;
    private List<Customer> items = null;
    private Customer selected = new Customer();
    

    public boolean isAddedBook(Book book) {
        return bookCustomerFacade.findByPrimaryKey(book.getIdBook(), SessionUtils.getUserId()).size() > 0;
    }

    public void addBook(Book book) {
        bookCustomerFacade.create(new Bookcustomer(new BookcustomerPK(SessionUtils.getUserId(),
                book.getIdBook()),
                new Date()));
    }
    public void deleteBook(Book book){
        /*Bookcustomer bk = bookCustomerFacade.findByPrimaryKey(book.getIdBook(), SessionUtils.getUserId()).get(0);
        bookCustomerFacade.remove(bk);*/
        bookCustomerFacade.deleteByKeys(book.getIdBook(), SessionUtils.getUserId());
    }

    /**
     * @return the selected
     */
    public Customer getSelected() {
        return selected;
    }

    /**
     * @param selected the selected to set
     */
    public void setSelected(Customer selected) {
        this.selected = selected;
    }

    private CustomerFacade getFacade() {
        return ejbFacade;
    }

    public List<Customer> getItems() {
        items = getFacade().findAll();
        return items;
    }

    public String create() {
        if (selected.getRegistrationdate() == null) {
            selected.setRegistrationdate(new Date());
        }
        if (selected != null) {
            getFacade().create(selected);
            loginBean.setLogin(selected.getEmail());
            loginBean.setPassword(selected.getPassword());
            loginBean.login();
            return "welcomePrimefaces";
        }
        return "registration";
    }

    public void update() {
        if (selected != null) {
            getFacade().edit(selected);
        }
    }

    public void destroy() {
        if (selected != null) {
            getFacade().remove(selected);
        }
        selected = null;
        items = null;
    }

    public Customer getCustomer(Integer id) {
        return getFacade().find(id);
    }

}
