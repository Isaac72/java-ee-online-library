/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lib.jsf;

import com.lib.jpa.Country;
import com.lib.jpaFacade.CountryFacade;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

/**
 *
 * @author Евгений
 */
@Named
@SessionScoped
public class CountryController implements Serializable {

    @EJB
    private com.lib.jpaFacade.CountryFacade ejbFacade;
    private List<Country> items = null;
    private Country selected = new Country();

    /**
     * @return the selected
     */
    public Country getSelected() {
        return selected;
    }

    /**
     * @param selected the selected to set
     */
    public void setSelected(Country selected) {
        this.selected = selected;
    }

    private CountryFacade getFacade() {
        return ejbFacade;
    }

    public List<Country> getItems() {
        if (items == null) {
            items = getFacade().findAll();
        }
        return items;
    }

    public void create() {
        if (selected != null) {
            getFacade().create(selected);
        }
        if (!isValidationFailed()) {
            items = null;
        }
    }

    public void update() {
        if (selected != null) {
            getFacade().edit(selected);
        }
    }

    public void destroy() {
        if (selected != null) {
            getFacade().remove(selected);
        }
        if (!isValidationFailed()) {
            selected = null;
            items = null;
        }
    }

    public Country getCountry(Integer id) {
        return getFacade().find(id);
    }

    private static boolean isValidationFailed() {
        return FacesContext.getCurrentInstance().isValidationFailed();
    }
}
