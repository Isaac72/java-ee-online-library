/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lib.jsf;

import com.lib.jpa.Author;
import com.lib.jpaFacade.AuthorFacade;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

/**
 *
 * @author Евгений
 */
@Named
@SessionScoped
public class AuthorController implements Serializable{
    @EJB
    private com.lib.jpaFacade.AuthorFacade ejbFacade;
    private List<Author> items = null;
    private Author selected = new Author();

    
   
    /**
     * @return the selected
     */
    public Author getSelected() {
        return selected;
    }

    /**
     * @param selected the selected to set
     */
    public void setSelected(Author selected) {
        this.selected = selected;
    }
    
    private AuthorFacade getFacade(){
        return ejbFacade;
    }
    
    public List<Author> getItems(){
        if (items == null){
            items = getFacade().findAll();
        }
        return items;
    }
    
    public void create(){
        if (selected != null){
            getFacade().create(selected);
        }
        if (!isValidationFailed()){
            items = null;
        }
    }
    
    public void update(){
        if (selected != null){
            getFacade().edit(selected);
        }
    }
    
    public void destroy(){
        if (selected != null){
            getFacade().remove(selected);
        }
        if (!isValidationFailed()){
            selected = null;
            items = null;
        }
    }
    
    public Author getAuthor(Integer id){
        return getFacade().find(id);
    }
    
    
    private static boolean isValidationFailed(){
        return FacesContext.getCurrentInstance().isValidationFailed();
    }
}
