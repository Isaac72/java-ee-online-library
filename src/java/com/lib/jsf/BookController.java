/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lib.jsf;

import com.lib.jpa.Book;
import com.lib.jpa.Bookcustomer;
import com.lib.jpaFacade.BookFacade;
import com.lib.namedBean.SessionUtils;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;


/**
 *
 * @author Евгений
 */
@Named
@SessionScoped
public class BookController implements Serializable {

    @EJB
    private com.lib.jpaFacade.BookFacade ejbFacade;
    @EJB
    private com.lib.jpaFacade.BookcustomerFacade bookCustomerFacade;
    private List<Book> items = null;
    private Book selected = new Book();
    private List<Book> customerBooks = null;
    private int genreFilter = 0;
    private int countryFilter = 0;
    private List<Book> filterBook;
    private int book;
    
    public int getBook(){
        return getItems().get(4).getAuthorId().getCountryId().getIdCountry();
    }

    public void resetFilters() {
        genreFilter = 0;
        countryFilter = 0;
    }

    public void filterCollection() {
        filterBook = new ArrayList<>(getItems());
        for (int i = 0; i < filterBook.size(); i++) {
            if (genreFilter != 0 && filterBook.get(i).getGenreId().getIdGenre() != genreFilter) {
                filterBook.remove(i);
                i--;
            }
            else if (countryFilter != 0 && filterBook.get(i).getAuthorId().getCountryId().getIdCountry() != countryFilter) {
                filterBook.remove(i);
                i--;
            }
        }
    }

    public List<Book> getFilterBook() {
        if (genreFilter == 0 && countryFilter == 0) {
            return getItems();
        }
        return filterBook;
    }

    public int getCountryFilter(){
        return this.countryFilter;
    }
    
    public void setCountryFilter(int country){
        this.countryFilter = country;
    }
    
    public void setGenreFilter(int genre) {
        this.genreFilter = genre;
    }

    public int getGenreFilter() {
        return genreFilter;
    }

    public List<Book> getCustomerItems() {
        customerBooks = new ArrayList<Book>();

        List<Bookcustomer> list = bookCustomerFacade.findAll();

        for (Bookcustomer bk : list) {
            if (bk.getBookcustomerPK().getCustomerId() == SessionUtils.getUserId()) {
                customerBooks.add(getBook(bk.getBookcustomerPK().getBookId()));
            }
        }
        return customerBooks;
    }

    /**
     * @return the selected
     */
    public Book getSelected() {
        return selected;
    }

    /**
     * @param selected the selected to set
     */
    public void setSelected(Book selected) {
        this.selected = selected;
    }

    private BookFacade getFacade() {
        return ejbFacade;
    }

    public List<Book> getItems() {
        items = getFacade().findAll();
        return items;
    }

    public void create() {
        if (selected != null) {
            getFacade().create(selected);
        }
        if (!isValidationFailed()) {
            items = null;
        }
    }

    public void update() {
        if (selected != null) {
            getFacade().edit(selected);
        }
    }

    public void destroy() {
        if (selected != null) {
            getFacade().remove(selected);
        }
        if (!isValidationFailed()) {
            selected = null;
            items = null;
        }
    }

    public Book getBook(Integer id) {
        return getFacade().find(id);
    }

    private static boolean isValidationFailed() {
        return FacesContext.getCurrentInstance().isValidationFailed();
    }
}
