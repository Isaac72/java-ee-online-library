/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lib.jsf;

import com.lib.jpa.Genre;
import com.lib.jpaFacade.GenreFacade;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

/**
 *
 * @author Евгений
 */
@Named
@SessionScoped
public class GenreController implements Serializable {

    @EJB
    private com.lib.jpaFacade.GenreFacade ejbFacade;
    private List<Genre> items = null;
    private Genre selected;

    /**
     * @return the selected
     */
    public Genre getSelected() {
        if (selected == null){
            selected = new Genre();
        }
        return selected;
    }

    /**
     * @param selected the selected to set
     */
    public void setSelected(Genre selected) {
        this.selected = selected;
    }

    private GenreFacade getFacade() {
        return ejbFacade;
    }

    public List<Genre> getItems() {
        items = getFacade().findAll();
        return items;
    }

    public void create() {
        if (selected != null) {
            getFacade().create(selected);
        }
    }

    public void update() {
        if (selected != null) {
            getFacade().edit(selected);
        }
    }

    public void destroy() {
        if (selected != null) {
            getFacade().remove(selected);
        }
        if (!isValidationFailed()) {
            selected = null;
            items = null;
        }
    }

    public Genre getGenre(Integer id) {
        return getFacade().find(id);
    }

    private static boolean isValidationFailed() {
        return FacesContext.getCurrentInstance().isValidationFailed();
    }

}
