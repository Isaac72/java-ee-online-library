package com.lib.jpa;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Book.class)
public abstract class Book_ {

	public static volatile SingularAttribute<Book, Genre> genreId;
	public static volatile SingularAttribute<Book, String> name;
	public static volatile SingularAttribute<Book, Integer> idBook;
	public static volatile CollectionAttribute<Book, Bookcustomer> bookcustomerCollection;
	public static volatile SingularAttribute<Book, Author> authorId;
	public static volatile SingularAttribute<Book, Date> adddate;

}

