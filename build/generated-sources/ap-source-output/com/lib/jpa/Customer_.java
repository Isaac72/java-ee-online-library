package com.lib.jpa;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Customer.class)
public abstract class Customer_ {

	public static volatile SingularAttribute<Customer, String> password;
	public static volatile SingularAttribute<Customer, Date> registrationdate;
	public static volatile CollectionAttribute<Customer, Bookcustomer> bookcustomerCollection;
	public static volatile SingularAttribute<Customer, String> email;
	public static volatile SingularAttribute<Customer, Integer> idCustomer;
	public static volatile SingularAttribute<Customer, String> realname;

}

