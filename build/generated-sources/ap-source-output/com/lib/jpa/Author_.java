package com.lib.jpa;

import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Author.class)
public abstract class Author_ {

	public static volatile SingularAttribute<Author, Integer> idAuthor;
	public static volatile CollectionAttribute<Author, Book> bookCollection;
	public static volatile SingularAttribute<Author, String> fio;
	public static volatile SingularAttribute<Author, Country> countryId;

}

