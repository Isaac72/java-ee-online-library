package com.lib.jpa;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Bookcustomer.class)
public abstract class Bookcustomer_ {

	public static volatile SingularAttribute<Bookcustomer, BookcustomerPK> bookcustomerPK;
	public static volatile SingularAttribute<Bookcustomer, Book> book;
	public static volatile SingularAttribute<Bookcustomer, Date> adddate;
	public static volatile SingularAttribute<Bookcustomer, Customer> customer;

}

