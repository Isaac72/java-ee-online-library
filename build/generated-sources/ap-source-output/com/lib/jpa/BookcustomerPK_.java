package com.lib.jpa;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(BookcustomerPK.class)
public abstract class BookcustomerPK_ {

	public static volatile SingularAttribute<BookcustomerPK, Integer> customerId;
	public static volatile SingularAttribute<BookcustomerPK, Integer> bookId;

}

